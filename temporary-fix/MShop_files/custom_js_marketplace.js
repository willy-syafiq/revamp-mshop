//@gs
    $(function () {
        $("#emagzs").select2({
            maximumSelectionLength: 3,
        });
    });
//iqbal
$(".addition_source_info_form").hide();
$('.status_account_type').change(function(e) {
   if($(this).val()==1){
        $('.corporateField').hide()
        $('.corporateField').addClass('hide')
    }else{
        $('.corporateField').show();
        $('.corporateField').removeClass('hide');
    }
});

// Created by Adrian
function passwordMatchChecker(password,retype_password){
    if(password.length==0 || retype_password.length==0){
        return false;
    }else{
        if(password==retype_password){
            $('.notif-password-checkout').addClass('hide');
            return true;
        }else{
            $('.notif-password-checkout').removeClass('hide');
            return false;
        }
    }
}
function checkoutRequiredKeyup(place="inCheckout"){
  inputCheckoutStatus=true;
  //check password and retype
 signUpTo = $('input[name="signup_to"]').is(":checked");
 // console.log("before Password"+signUpTo+place);
 if((signUpTo && place =='inCheckout') || (place!='inCheckout')){
    password           = $('input[name="password"]').val();
    retype_password    = $('input[name="retype-password"]').val();
     if(! passwordMatchChecker(password,retype_password)){
         inputCheckoutStatus=false;
     }

 }

 // check status personal/corprate field is filled
 corporateCheck = $("select.status").val();
 if(corporateCheck==0){
  $(".job1 input,.job1 select").each(function(){
    if ($(this).val().length==0) {
      inputCheckoutStatus=false;
    }
  });
 }
 // Check each required field is filled
 if(place=="inCheckout"){
    $('#checkoutForm').find('input:required,textarea:required,select:required').each(function() {
    thisVal = $(this).val();
    if(thisVal!=null){
      if($(this).val().length==0){
          inputCheckoutStatus=false;
      }
    }
  });
 }else if(place=='inRegister'){
 $('#registerForm').find('input:required,textarea:required,select:required').each(function() {
    thisVal = $(this).val();
     // console.log($(this).attr('name')+" : "+$(this).val());
    if(thisVal!=null){
      if($(this).val().length==0){
          inputCheckoutStatus=false;
      }
    }
  });
 }

  if(inputCheckoutStatus){
     if(place=="inRegister"){
        $(".regSubmitBtn").prop("disabled",false);
     }else if(place=='inCheckout'){
        $("#checkoutBtn").html("PROCESS");
        $("#checkoutBtn").prop("disabled",false);
     }
  }
  else{
     if(place="inRegister"){
        $(".regSubmitBtn").prop("disabled",true);
     }else if(place=='inCheckout'){
        $("#checkoutBtn").html("Checking Fields...");
        $("#checkoutBtn").prop("disabled",true);
     }
   }
}
// ******

/**
 * Created by Farhad on 10/06/2016.
 */
$(document).ready(function() {
    if($('.status_account_type').val()==1){
        $('.corporateField').addClass('hide')
    }
    $('.status_account_type').trigger('change');
    var base_url = $('input[name=base_url]').val();
    var notif_option = {
        type: 'success',
        timer: 5000,
        delay: 1000,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    };
    // Adjust Footer behaviour deoends on html size
    var browserHeight = $(window).height();
    var HtmlHeight = $('html').height();
    if (HtmlHeight < browserHeight) {
        $(".footerWrapper").css({'position': 'fixed', 'bottom': 0, 'width': 100 + '%'});
    }
    $('.check_redeem').click(function () {
        var loyalty_member_benefit_id = $(this).attr('loyalty-member-benefit-id');
        $.ajax({
            type: 'POST',
            data: {loyalty_member_benefit_id: loyalty_member_benefit_id},
            dataType: "json",
            cache : false,
            url: base_url+'Member/check_already_redeem',
            success: function (data) {
                if(data.status == 'success') {
                 modal_content('confirm_modal/use_benefit');
                 var timer = setTimeout(function() {
                    window.location=base_url+data.url;
                }, 2000);
                }else{
                    modal_content('confirm_modal/already_use');
                }
            }
        });

    });

    // **************
    /* Adjust hover product image height*/
    adjustHoverProd();
    $(window).resize(function () {
        adjustHoverProd();
    });
    function adjustHoverProd() {
        buyNowAttr = $('.buynow-hover').attr('style');
        if (buyNowAttr) {
            prodImgHeight = $('.prod-image').innerHeight();
            prodImgWidth = $('.prod-image').innerWidth();
            $('.buynow-hover').attr('style', buyNowAttr + 'height:' + prodImgHeight + 'px;' + 'width:' + prodImgWidth + 'px');
        }
    }

    /*Confirmation email modal*/
    $('#checkoutBtn').click(function (e) {
        e.preventDefault();
        email = $('input[name="email[]"]').val();
        $('.emailToConfirmation').text(email);
        var $modal=$('#modal_confirm');
        $modal.foundation('open');
    });

    $('.emailConfirmYes').click(function (e) {
        e.preventDefault();
        $('#checkoutBtnReal').click();
    });
    $('.emailConfirmNo').click(function (e) {
        e.preventDefault();
        $('#confirmEmailCheckout').modal('hide');
        $('.emailBilling').focus();
    });
    /*End of confirmation email modal*/
    /*Make sure particiapnts email for ticket is unique*/
    function ticketGetEmailStats(name) {
        var ticketGethide = $('#ticketGethide').val();
        if (ticketGethide > 0) {
            //its ticket type
            var emailArr = [];
            $('input[name="email[]"]').each(function (i, e) {
                if (i > 0) {
                    eval = $(e).val();
                    if (eval) {
                        if ($.inArray(eval, emailArr) === -1) emailArr.push(eval);
                    }
                }
            });
            // console.log(emailArr.length);
            if (emailArr.length == ticketGethide) {
                //all email are unique
                $('.notif-email-unique').addClass('hide');
            } else {
                $('.notif-email-unique').removeClass('hide');
                $("#checkoutBtn").prop("disabled", true);
                //make process disaled
            }
            notEmpty = 0;
            $('input[name="email[]"]').each(function (i, e) {
                eval = $(e).val();
                if (eval == "") {
                    $(e).next('p').addClass('hide');
                } else {
                    if (i) {
                        notEmpty++;
                    }
                }
            });
            if (notEmpty == 1) {
                $('.notif-email-unique').addClass('hide');
            }
        }
    }

    $('input[name="add_secertary"]').click(function () {
        // console.log($(this).parents('.cell').next('.email_secertary_container').html());
        if (this.checked) {
            $(this).parents('.cell').next('.email_secertary_container').removeClass('hide');
        } else {
            $(this).parents('.cell').next('.email_secertary_container').addClass('hide');
        }
    });
    // alert delay hide
    // $(".alert").delay(4500).slideUp("slow");
    //Select2 for sidebar filter widget
    $('.select2-widget').each(function (index, element) {
        $('.' + $(element).attr('data-id')).select2({
            placeholder: '' + $(element).attr('data-name'),
        });
    });
    $('.company-select2').select2({
        ajax: {
            url: base_url + "Landing/getCompany",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term,
                    page: params.page
                }
                return query;
            },
            processResults: function (data) {
                return {
                    results: data.items
                };
            },
        }
    });
    // $('.cart-btn:not(:disabled)').click(function(e){
    //     console.log($(this).attr('class') + "tes");
    // });
    //overide checkout button to show modal
    $('.cart-btn').click(function (e) {
         e.preventDefault();
         if($('.agreeCB').is(":checked")) {
        $.ajax({
            url: base_url + 'member/ajaxIsMember',
            success: function (e) {
                if (e == 0) {
                    modal_content('confirm_modal/guest_login');
                } else {
                    window.location = base_url + "checkout";
                }
            }
        });
        }
    });
      $('.proceedButton').click(function (e) {
    e.preventDefault();
       if(document.getElementById('tnc').checked || document.getElementById('tnc_mobile').checked) {
            $.ajax({
                url: base_url + 'member/ajaxIsMember',
                success: function (e) {
                    if (e == 0) {
                      modal_content('confirm_modal/guest_login');
                    } else {
                        window.location = base_url + "checkout";
                    }
                }
            });
        }else{
            ownAlert("Please tick the terms & conditions box to proceed","", function() {});

        }
    });
    /*$('#proceedButton_mobile').click(function (e) {
    e.preventDefault();
       if(document.getElementById('tnc_mobile').checked) {
            $.ajax({
                url: base_url + 'member/ajaxIsMember',
                success: function (e) {
                    if (e == 0) {
                      modal_content('confirm_modal/guest_login');
                    } else {
                        window.location = base_url + "checkout";
                    }
                }
            });
        }else{
            ownAlert("Please tick the terms & conditions box to proceed","", function() {});

        }
    });*/
    //terms condition modal
    $('.termsCondition-trigger').click(function (e) {
        e.preventDefault();
        $('#termsCondition').modal();
    });
    // Subscribe with Email in header
    $(".newsletter_btn").click(function () {
        $(".form_subscribe").toggleClass("form_width");
    });
    $(".formSubscriber").submit(function (e) {
        e.preventDefault();
        var subscriberEmail = $(this).find("input[name=email_subscriber]").val();
        $(this).find("input[name=email_subscriber]").val('');
        $.ajax({
            type: 'POST',
            data: {email: subscriberEmail},
            url: base_url + 'admin/Newsletter/addSubscriber',
            success: function (response) {
                // console.log(response +"Tes");
                if (response == "success") {
                    document.location.href=base_url + 'landing/submit_subscribe';
                    // $.notify({
                    //     // options
                    //     icon: 'glyphicon glyphicon-info-sign',
                    //     message: 'Email has been subscribed succesfully, keep in touch with us'
                    // }, {
                    //     // settings
                    //     notif_option
                    // });
                    // $("..ressponse").html("sukses");
                } else {
                    document.location.href=base_url + 'landing/submit_subscribe';
                    // $.notify({
                    //     // options
                    //     icon: 'glyphicon glyphicon-info-sign',
                    //     message: 'Email has been subscribed on our system'
                    // }, {
                    //     // settings
                    //     type: 'warning',
                    //     timer: 5000,
                    //     delay: 1000,
                    //     animate: {
                    //         enter: 'animated fadeInDown',
                    //         exit: 'animated fadeOutUp'
                    //     }
                    // });
                    // $(".ressponse").html("gagal");
                }
            }
        });
    });
    //continue without login handler
    $(document).on("click",".cart-w-login",function(e) {
        e.preventDefault();
        links = $(this).attr('href');
        window.location = links;
    });
    $(document).on("click","#LoginCheckout_new",function(e) {
        e.preventDefault();
        var email = $('#checkout_email').val();
        var password = $('#checkout_password').val();
        $.ajax({
            type: 'POST',
            data: {email: email, password: password},
            beforeSend: function () {
                $('#cart-login-error').addClass('hide');
            },
            url: base_url + 'member/doLoginCheckout',
            success: function (response) {
                if (response == 'success') {
                    //location.reload();
                    $('#cart-login-error').addClass('hide');
                    window.location = base_url + 'checkout'
                } else {
                    $('div.warning-login').removeClass('hide');
                    $('div.warning-login').show();
                    //$("#login-error").css("display","block");
                    //$('#login-error').html('Invalid username and password');
                }
            }
        });
    });
    //Hover product card di Landing
    $(".buynow-hover-trigger").hover(
        function () {
            $(this).children('.buynow-hover').removeClass('hide');
        }, function () {
            $(this).children('.buynow-hover').addClass('hide');
        }
    );
    $('.iframe_report').height($('.report_featured_image').height());
    //header
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 139) {
            $('.navbar').addClass('navbar-fixed-top');
        } else {
            $('.navbar').removeClass('navbar-fixed-top');
        }
    });
    // Set Featured Image
    $('.setImage').click(function () {
        $('input[name="featuredImage"]').val($(this).attr('data-link'));
    });
    $('option').click(function () {
        var id = $(this).attr('data-id');
        //alert(id);
        var value = $(this).val();
        //alert($('option[value="' + value + '"]').data('data-id'));
    });
    //  add report to cart
    $('.add-cart-report').click(function () {
        // console.log($(this).attr('data-id'));
        $.ajax({
            type: 'POST',
            data: {id: $(this).attr('data-id'), parentID: $(this).attr('data-parent')},
            url: base_url + 'cart/insertCartNew',
            success: function (response) {
                $.notify({
                    // options
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Product has been inserted to cart'
                }, {
                    // settings
                    notif_option
                });
                // $('.response').html('Product has been inserted to cart ');
                // $('.response').slideDown('fast').delay('3000').slideUp('fast');
                $('.cart-button').html(response);
            }
        });
    });
    $('.add-to-cart').click(function () {
        // console.log($(this).attr('data-id'));
        $.ajax({
            type: 'POST',
            data: {id: $(this).attr('data-id'), qty: $('input[name=qty]').val()},
            url: base_url + 'cart/insertCartNew',
            success: function (response) {
                $.notify({
                    // options
                    icon: 'glyphicon glyphicon-info-sign',
                    url: base_url + 'cart',
                    target: '_blank',
                    message: 'Product has been inserted to cart'
                }, {
                    // settings
                    notif_option
                });
                // $('.response').html('Product has been inserted to cart ');
                // $('.response').slideDown('fast').delay('3000').slideUp('fast');
                $('.cart-button').html(response);
            }
        });
    });
    $('.add-to-cart2').click(function () {
        var id = "";
        var name = "";
        var qty = "";
        $('input[name="qty[]"]').each(function () {
            qty = qty + $(this).val() + "|";
        });
        $('input[name="product_name[]"]').each(function () {
            name = name + $(this).val() + "|"
        });
        $('input[name="product_id[]"]').each(function () {
            id = id + $(this).val() + "|";
        });
        // console.log(qty);
        $.ajax({
            type: 'POST',
            data: {id: id, qty: qty, name: name},
            url: base_url + 'cart/insertCartArray',
            success: function (response) {
                $.notify({
                    // options
                    icon: 'glyphicon glyphicon-info-sign',
                    url: base_url + 'cart',
                    target: '_blank',
                    message: 'Product has been inserted to cart'
                }, {
                    // settings
                    notif_option
                });
                // $('.response').html('Product has been inserted to cart ');
                // $('.response').slideDown('fast').delay('3000').slideUp('fast');
                $('.cart-button').html(response);
            }
        });
    });
    $('.update-cart').click(function () {
        var qty_sent = $(this).attr('data-qty');
        var row_data = $(this).attr('data-row');
        var promocode_allProd = $(".promocode_allProd").attr('value');
        if ($(this).attr('data-qty') == 999) {
            if ($("input." + row_data).val() != $("input.qty_temp" + row_data).val()) {
                qty_sent = $("input." + row_data).val() - $("input.qty_temp" + row_data).val();
            }
            else {
                qty_sent = 0;
            }
            $.ajax({
                type: 'POST',
                data: {id: $(this).attr('data-id'), qty: qty_sent, rowid: row_data},
                url: base_url + 'cart/updateCart',
                success: function (response) {
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-info-sign',
                        url: base_url + 'cart',
                        target: '_blank',
                        message: 'Please wait, your cart is being updated'
                    }, {
                        // settings
                        notif_option
                    });
                    // $('.response').html('Cart has been updated');
                    // $('.response').slideDown('fast').delay('3000').slideUp('fast');
                    location.reload();
                }
            });
        }
        else if ($(this).attr('data-qty') == -404) {
            confirm("Are you sure want to delete this item?","", function() {
               $.ajax({
                   type: 'POST',
                   data: {id: $(this).attr('data-id'), qty: qty_sent, rowid: row_data},
                   url: base_url + 'cart/updateCart',
                   success: function (response) {
                       // $.notify({
                       // // options
                       //    icon: 'glyphicon glyphicon-info-sign',
                       //    message: 'Cart has been updated'
                       // },{
                       // // settings
                       //    notif_option
                       // });
                       $('.response').html('Cart has been updated');
                       $('.response').slideDown('fast').delay('3000').slideUp('fast');
                   }
               });
               $.ajax({
                   type: 'POST',
                   data: {promocode_all: promocode_allProd},
                   url: base_url + 'admin/promocode/updatePromocode_allProd',
                   success: function (response) {
                       location.reload();
                   }
               });
            });
        }
    });

    function confirm(title, message, callback) {
        // create your modal template
        var modal =
                     '<div class="reveal padding-2" id="confirmation">'+
                         '<h2>'+title+'</h2>'+
                     '<h1 class="lead">'+message+'</h1>'+
                     '<div class="grid-x flex-container align-right">'+
                         '<button class="button cancel-button hvr-sweep-to-right margin-right-1 no-bot-margin" data-close>No</button>'+
                         '<button class="button marketeers hvr-sweep-to-right yes no-bot-margin" id="yes">Yes</button>'+
                     '</div>'+
                     '<button class="close-button" data-close aria-label="Close modal" type="button">'+
                         '<span aria-hidden="true">&times;</span>'+
                     '</button>'
                     ;
        // appending new reveal modal to the page
        $('body').append(modal);
        // registergin this modal DOM as Foundation reveal
        var confirmation = new Foundation.Reveal($('#confirmation'));
        // open
        confirmation.open();
        // listening for yes click

        $('#yes').on('click', function() {
            // close and REMOVE FROM DOM to avoid multiple binding
            confirmation.close();
            $('#confirmation').remove();
        // calling the function to process
        callback.call();
        });
        $(document).on('closed.zf.reveal', '#confirmation', function() {
            // remove from dom when closed
            $('#confirmation').remove();
        });

    }
    isbenefiteactive();
    function isbenefiteactive(){
        $.ajax({
        url: base_url+'member/isbenefitactive',
        success: function (response) {
            var data = JSON.parse(response);
            if(data.status=="active"){
                $('select[name=cart-qty]').attr('disabled', true);
                $( ".removedCartBtn" ).remove();
            }
        }
    })
    };
    // Update cart quantity
    $('select[name=cart-qty]').change(function () {
        var rowid = $(this).attr('data-rowid');
        var value = $(this).val();
        var promocode_allProd = $(".promocode_allProd").attr('value');
        isbenefiteactive();
        $.ajax({
            type: 'POST',
            data: {qty: value, rowid: rowid},
            url: base_url + 'cart/updateQtyCart',
            beforeSend: function () {
                $(".cart_updating").show();
                $(".cart-btn").addClass("disabled");
                $('.cart-btn').attr("disabled", true);
            },
            success: function (response) {
                var data = JSON.parse(response);
                $('#total').html(data.total);
                $('#subtotal' + rowid).html(data.subTotal);
                $.ajax({
                    type: 'POST',
                    data: {promocode_all: promocode_allProd},
                    url: base_url + 'admin/promocode/updatePromocode_allProd',
                    success: function (response) {
                        $(".cart_updating").hide();
                        cartBtn_enabled();
                        // var res = JSON.parse(response);
                        // $("#promocode_disc").html("<b><font color='#ec008c'><i>" + res.data + "</i></font></b>");
                        $.ajax({
                            type: 'POST',
                            data: {qty: value, rowid: rowid},
                            url: base_url + 'cart/updateQtyCart',
                            success: function (response) {
                                var data = JSON.parse(response);
                                $('#total').html(data.total);
                                $('#subtotal' + rowid).html(data.subTotalRow);
                                $('#subTotal').html(data.subTotal);
                            }
                        })
                    }
                });
            }
        })
    });
    $('#not-use-balance').change(function () {
        if (this.checked) {
            $('input[name=balance]').attr('disabled', true);
        }
    });
    $('#use-balance').change(function () {
        if (this.checked) {
            $('input[name=balance]').removeAttr('disabled');
        }
    });
    $('input[name=balance]').click(function () {
        var subTotal = $('#subTotal');
        var shippingCost = $('#shippingCost');
        var tax = $('#tax');
        var balance = $('#balanceValue');
        var total = $('#total');
        if (this.checked) {
            $.ajax({
                type: 'GET',
                url: base_url + 'Payment/useBalance',
                success: function (response) {
                    var obj = JSON.parse(response);
                    $('#oriPrice').css({'text-decoration': 'line-through', 'font-style': 'italic '});
                    subTotal.append(" " + obj.cut_subTotal);
                    shippingCost.html(obj.cut_shipping);
                    balance.html("Your Balance : " + obj.cut_balance);
                    total.html(obj.cut_total);
                }
            });
        } else {
            $.ajax({
                type: 'GET',
                url: base_url + 'payment/unuseBalance',
                success: function (response) {
                    var obj = JSON.parse(response);
                    subTotal.html(obj.subTotal);
                    shippingCost.html(obj.shippingCost);
                    tax.html(obj.tax);
                    balance.html("Your Balance : " + obj.balance);
                    total.html(obj.total);
                }
            });
        }
    });
// Input masking by Adrian - 23 Juli 2016
    $(":input").inputmask();
// *************
// Sticky Bar by Adrian 22 Juli 2016
    $('#sidebar, .side-profile-wrapper').theiaStickySidebar({
        // Settings
        additionalMarginTop: 70,
        additionalMarginBottom: 0,
    });
    $('.summary-wrapper').theiaStickySidebar({
        // Settings
        additionalMarginTop: 80,
        additionalMarginBottom: 0,
    });
// *************
// Tolltip Bootstrap initialization
    $('[data-tooltip="tooltip"]').tooltip();
// ****
    //  add to cart
    // $('.add-to-cart').click(function(){
    //    var product_id=[];
    //    var product_name=[];
    //    var product_price=[];
    //    var qty=[];
    //    var product_type=[];
    //    $('input[name=product_id]').each(function(index, value){
    //       product_id.push($(this).val());
    //    });
    //    $('input[name=product_name]').each(function(index, value){
    //       product_name.push($(this).val());
    //    });
    //    $('input[name=product_price]').each(function(index, value){
    //       product_price.push($(this).val());
    //    });
    //    $('input[name=qty]').each(function(index, value){
    //       qty.push($(this).val());
    //    });
    //    $('input[name=product_type]').each(function(index, value){
    //       product_type.push($(this).val());
    //    });
    //    console.log(product_id);
    //    $.ajax({
    //       type:'POST',
    //       data:{product_id:product_id,product_name:product_name,product_price:product_price,qty:qty,product_type:product_type},
    //       url:base_url+'cart/insertCart',
    //       success:function(response){
    //          $('.response').html('Product has been inserted to cart ');
    //          $('.response').slideDown('fast').delay('3000').slideUp('fast');
    //          $('.cart-button').html(response);
    //       }
    //    });
    // });
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        var email = $('input[name=email]').val();
        var password = $('input[name=loginPassword]').val();
        $.ajax({
            type: 'POST',
            data: {email: email, password: password},
            url: base_url + 'member/doLoginCheckout',
            complete: function () {
                $('input[name=email]').attr('disabled', false);
                $('input[name=loginPassword]').attr('disabled', false);
                // $(".loginEmail_loading").hide();
            },
            success: function (response) {
                if (response == 'success') {
                    window.location = base_url + "member/dashboard";
                    $("#login-error").hide(200);
                    $("#login-success").show(200).delay(200);
                    $('#login-success').html("<span class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span> Ready to Sign in, Please Wait")
                } else {
                    $('div.login_alert').removeClass('hide');
                    $('div.login_alert').show();
                }
            }
        });
    });
    $('#loginFormMobile').submit(function (e) {
        e.preventDefault();
        var email = $('input[name=emailMobile]').val();
        var password = $('input[name=loginPasswordMobile]').val();
        $.ajax({
            type: 'POST',
            data: {email: email, password: password},
            url: base_url + 'member/doLoginCheckout',
            complete: function () {
                $('input[name=emailMobile]').attr('disabled', false);
                $('input[name=loginPasswordMobile]').attr('disabled', false);
            },
            success: function (response) {
                if (response == 'success') {
                    window.location = base_url + "member/dashboard";
                    $("#login-error").hide(200);
                    $("#login-success").show(200).delay(200);
                    $('#login-success').html("<span class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span> Ready to Sign in, Please Wait")
                } else {
                    alert(email);
                    alert(password);
                    $('div.login_alert').removeClass('hide');
                    $('div.login_alert').show();
                }
            }
        });
    });

    // convert number to rupiah
    function idrFormatNumber(number) {
        number = parseInt(number, 10);
        number = number.toString();
        number = number.split('').reverse().join('');
        var result = '';
        for (i = 0; i < number.length; i++) {
            result += number[i];
            if ((i + 1) % 3 === 0 && i !== (number.length - 1)) {
                result += '.';
            }
        }
        result = result.split('').reverse().join('');
        return result;
    }

    $('.ticketQty').change(function () {
        //var numRows=$('input[name=totalRow]').val()-1;
        var numRows = $('input[name=totalRow]').val(); // by Tisna
        var total = 0;
        for (var i = 0; i < numRows; i++) {
            // console.log('i:'+i);
            var price = $('input[name=price' + i + ']').val();
            var qty = $('#qty' + i + ' option:selected').val();
            var totalPrice = $('#totalPrice' + i);
            if (!qty) {
                qty = 0;
            }
            if (price == null) {
                price = 0;
            }
            totalPriceVal = idrFormatNumber(qty * price);
            if (totalPriceVal == 0) {
                totalPriceVal = 0;
            } else {
                totalPriceVal = 'IDR ' + totalPriceVal;
            }
            totalPrice.text(totalPriceVal);
            total += qty * price;
        }
        $('#subTotal').html('IDR ' + idrFormatNumber(total));
        // console.log($('#ticket').serialize());
    });
    //disable payment button
//    $('#tabs').tab();


    checkoutRequiredKeyup();
    function checkoutRequiredKeyup(place = 'inCheckout') {
        inputCheckoutStatus = true;
        // console.log("initial"+inputCheckoutStatus);
        //check password and retype
        signUpTo = $('input[name="signup_to"]').is(":checked");
        // console.log("before Password"+signUpTo+place);
        if ((signUpTo && place == 'inCheckout') || (place != 'inCheckout')) {
            password = $('input[name="password"]').val();
            retype_password = $('input[name="retype-password"]').val();
            // alert(password+'password');
            // alert(retype_password+'retype_password');
            if (password.length == 0 || retype_password.length == 0) {
                inputCheckoutStatus = false;
            } else {
                if (password == retype_password) {
                    $('.notif-password-checkout').addClass('hide');
                } else {
                    $('.notif-password-checkout').removeClass('hide');
                    inputCheckoutStatus = false;
                }
            }
        }
        // console.log("afterPassword"+inputCheckoutStatus);
        // check status personal/corprate field is filled
        corporateCheck = $("select.status").val();
        if (corporateCheck == 0) {
            $(".job1 input,.job1 select").each(function () {
                console.log("Corporate" + $(this).val());
                if ($(this).val().length == 0) {
                    inputCheckoutStatus = false;
                }
            });
        }
        // Check each required field is filled
        if (place == "inCheckout") {
            $('#checkoutForm').find('input:required,textarea:required,select:required').each(function () {
                thisVal = $(this).val();
                // console.log($(this).attr('name')+" : "+$(this).val());
                if (thisVal != null) {
                    if ($(this).val().length == 0) {
                        inputCheckoutStatus = false;
                    }
                }
                // console.log(inputCheckoutStatus+" : "+$(this).attr('name')+$(this).val().length);
            });
        } else if (place == 'inRegister') {
            $('#registerForm').find('input:required,textarea:required,select:required').each(function () {
                thisVal = $(this).val();
                // console.log($(this).attr('name')+" : "+$(this).val());
                if (thisVal != null) {
                    if ($(this).val().length == 0) {
                        inputCheckoutStatus = false;
                    }
                }
                // console.log(inputCheckoutStatus+" : "+$(this).attr('name')+$(this).val().length);
            });
        }
        if (inputCheckoutStatus) {
            // console.log(place+"this enabled"+inputCheckoutStatus);
            if (place == "inRegister") {
                $(".regSubmitBtn").prop("disabled", false);
            } else if (place == 'inCheckout') {
                $("#checkoutBtn").prop("disabled", false);
            }
        }
        else {
            if (place = "inRegister") {
                $(".regSubmitBtn").prop("disabled", true);
            } else if (place == 'inCheckout') {
                $("#checkoutBtn").prop("disabled", true);
            }
        }
    }

    $('#checkoutForm select, #checkoutForm input[type="checkbox"]').change(function () {
        checkoutRequiredKeyup();
        ticketGetEmailStats($(this).attr('name'));
    });
    $('#checkoutForm input, #checkoutForm textarea,#checkoutForm input[type="password"]').keyup(function () {
        checkoutRequiredKeyup();
        ticketGetEmailStats($(this).attr('name'));
    });
    $('#checkoutForm input[name="email[]"]').bind("paste", function () {
        checkoutRequiredKeyup();
        ticketGetEmailStats($(this).attr('name'));
    });
    $('.registerForm input, .registerForm textarea, .registerForm input[type="password"]').keyup(function () {
        checkoutRequiredKeyup('inRegister');
    });
    $("#changePasswordForm input[name='new_password'], #changePasswordForm input[name='retype_new_password']").keyup(function () {
        password = $("input[name='new_password']").val();
        retype_password = $('input[name="retype_new_password"]').val();
        if (passwordMatchChecker(password, retype_password)) {
            $(".updatePasswSubmit").prop("disabled", false);
        } else {

            $(".updatePasswSubmit").prop("disabled", true);
        }
    });
    // Terms and condition check

    //$("button.cart-btn").addClass("disabled"); @modified 26-02-2018
    //$("button.cart-btn").attr("disabled", true); @modified 26-02-2018
    function cartBtn_enabled() {
        var termsCheck = $("input[name='termsAgreedCheck']");
        if (termsCheck.is(':checked')) {
            $("button.cart-btn").removeClass("disabled");
            $("button.cart-btn").removeAttr("disabled");
        } else {
            $("button.cart-btn").addClass("disabled");
            $("button.cart-btn").attr("disabled", true);
        }

    }

    $("input[name='termsAgreedCheck']").prop('checked', false);
    $("input[name='termsAgreedCheck']").change(function () {
        cartBtn_enabled();
    });
    // Copy Billing address to Shipping Address value
    $(".copy-billing").change(function () {
        if ($(this).is(":checked")) {
            var bill_fullname = $("input[name='fullname[]']").val();
            var bill_email = $("input[name='email[]']").val();
            var bill_phone = $("input[name='phone[]']").val();
            var alamat = $("textarea[name='address[]']").val();
            var bill_postalcode = $("input[name='billing_postalcode']").val();// Added by Adrian Feb,21 2017
            // console.log(bill_fullname);
            $("input[name='shipping_fullname']").val(bill_fullname);
            $("input[name='shipping_email']").val(bill_email);
            $("input[name='shipping_phone']").val(bill_phone);
            $("textarea[name='shipping_address1']").val(alamat);
            $("input[name='shipping_postalcode']").val(bill_postalcode);
            $
        } else {
            $("input[name='shipping_fullname']").val("");
            $("input[name='shipping_email']").val("");
            $("input[name='shipping_phone']").val("");
            $("textarea[name='shipping_address1']").val("");
            $("input[name='shipping_postalcode']").val("");// Added by Adrian Feb,21 2017
        }
    });
    // change job status
    $('.status').change(function () {
        var c = ".corporate";
        if ($(this).val() == 0) {
            $(c).removeClass('hide');
            $(c).removeAttr('readonly').attr('required', true);
        } else {
            $(c).addClass('hide');
            $(c).attr('readonly', true).removeAttr('required');
        }
    });
    // Auto Check on page billing and shipping
    // $("input[name='signup_to']").attr('checked','checked');
    // $('.password_container').removeClass('hide');
    $('input[name="signup_to"]').click(function (e) {
        $('.password_container').toggleClass('hide');
    });
    // Toggle fly menu mobile
    $(".toggle-menu-mobile").click(function () {
        $(".main-wrapper").toggleClass("collapsing-body");
        $("#fly-wrap").toggleClass("collapsing-fly");
    });
    // ForgetPassword
    $(".ForgetPasswSubmit").click(function (e) {
        e.preventDefault();
        var ForgetPasswordEmail = $(".ForgetPasswordEmail").val();
        $.ajax({
            type: 'POST',
            data: {ForgetPasswordEmail: ForgetPasswordEmail},
            url: base_url + 'member/check_emailAjax',
            beforeSend: function () {
                $('.ForgetPasswSubmit').button("loading");
            },
            success: function (response) {
                var result = JSON.parse(response);
                $('.ForgetPasswSubmit').button("reset");
                if (result.status == "EmailNotFound") {
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: 'Your email is not found in our list'
                    }, {
                        // settings
                        type: 'warning',
                        timer: 5000,
                        delay: 1000,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                } else {
                    $("#ForgetPasswordForm").submit();
                }
            }
        });
    });
    // Register Member

    $('.regSubmitBtn').click(function (e) {
        e.preventDefault();
        var regEmail = $('.regEmail').val();
        $.ajax({
            type: 'POST',
            data: {ForgetPasswordEmail: regEmail},
            url: base_url + 'member/is_emailExistedAjax',
            beforeSend: function () {
                $('.regSubmitBtn').button('loading');
            },
            success: function (response) {
                var result = JSON.parse(response);
                if (result.status == "EmailFound") {
                    $('.regSubmitBtn').button('reset');
                    $('.register-alert').hide(200);
                    $('.register-alert').show(200).delay(200);
                } else {
                    $('.register-alert').hide(200);
                    $('.register-success').show(200);
                    $("#registerFormCheck").submit();
                }
            }
        });
    });
    // **********
    // confirmation Promocode
    $('#submitPromocode').click(function () {
        var code = $('input[name=promocode]').val();
        $.ajax({
            type: 'POST',
            data: {code: code},
            url: base_url + 'admin/promocode/checkPromocodeAjax',
            success: function (response) {
                var result = JSON.parse(response);
                if (result.status) {
                    Lobibox.confirm({
                        msg: "This promocode have a minimum purchase, are you sure will use this ?",
                        modal: true,
                        callback: function ($this, type, ev) {
                            if (type == 'yes') {
                                $("#submitPromocode").button("loading");
                                $('#promocode').submit();
                            }
                            else {
                                return false;
                            }
                        }
                    });
                } else {
                    Lobibox.confirm({
                        msg: "Are you sure you want to use this Promocode ?",
                        modal: true,
                        callback: function ($this, type, ev) {
                            if (type == 'yes') {
                                $("#submitPromocode").button("loading");
                                $('#promocode').submit();
                            }
                            else {
                                return false;
                            }
                        }
                    });
                }
            }
        });
    });
    // Advance search Training
    $('#trainingType').change(function () {
        var val = $(this).val();
        // console.log(val);
        // $.ajax({
        //     type:'POST',
        //     data:{cat_parent:val},
        //     url:base_url+'landing/getCategoryTraining',
        //     beforeSend:function(e){
        //         $(".btnAdvSearch").button("loading");
        //     },
        //     success:function(response){
        //         $(".btnAdvSearch").button("reset");
        //         response=JSON.parse(response);
        //         // alert(response.status);
        //         $('#trainingProgram').find('option').remove();
        //         $('#trainingProgram').append($("<option></option>").attr("value","").text("Training Program"));
        //         if(response.status==1){
        //             // alert('jalan');
        //             console.log(response.data);
        //             $.each(response.data,function(i,obj){
        //                 $('#trainingProgram').append($("<option></option>").attr("value",obj.id).text(obj.name));
        //                 // console.log(obj.id);
        //             })
        //         }
        //     }
        // })
    });

    $('#trainingProgram').change(function () {
        var val = $(this).val();
        var name = $("#trainingCategory").find(':selected').data('name');
        // $.ajax({
        //     type:'POST',
        //     data:{cat_parent:val,cat_name:name},
        //     url:base_url+'landing/getCategoryTraining2',
        //     beforeSend:function(e){
        //         $(".btnAdvSearch").button("loading");
        //     },
        //     success:function(response){
        //         $(".btnAdvSearch").button("reset");
        //         response=JSON.parse(response);
        //         if(response.status==1){
        //             $('#catHidden').val(response.data);
        //         }
        //     }
        // })
    });
    $('#source_information').change(function () {
        var val = $(this).val();
        if(val=="other"){
              $("#other_info_form").show(200).delay(200);
              $("#sales_person_form").hide(200).delay(200);
        }
        else if(val=="sales_person"){
            $("#sales_person_form").show(200).delay(200);
            $("#other_info_form").hide(200).delay(200);
        }
        else{
            $(".addition_source_info_form").hide(200).delay(200);
        }
        return false;
        // $.ajax({
        //     type:'POST',
        //     data:{cat_parent:val,cat_name:name},
        //     url:base_url+'landing/getCategoryTraining2',
        //     beforeSend:function(e){
        //         $(".btnAdvSearch").button("loading");
        //     },
        //     success:function(response){
        //         $(".btnAdvSearch").button("reset");
        //         response=JSON.parse(response);
        //         if(response.status==1){
        //             $('#catHidden').val(response.data);
        //         }
        //     }
        // })
    });
    // Custom transaction in select product on Merchandise
    $(".sel-option-form, .opt-select").change(function () {
        var option_val = $(this).val().split("|");
        var prod_id = option_val[0];
        var prod_price = option_val[1];
        $(".selected-buy").attr('data-id', prod_id);
        $(".selected-price").html("IDR " + idrFormatNumber(prod_price));
        $.ajax({
            type: 'POST',
            data: {id: prod_id},
            url: base_url + 'landing/generateZoomedImage',
            success: function (response) {
                var data = JSON.parse(response);
                $("#zoom3").attr({
                    "src": base_url + "asset/upload/images/" + data.featured_image,
                    "data-zoom-image": base_url + "asset/upload/images/" + data.featured_image
                });
                var image_loc = "url(" + base_url + "asset/upload/images/" + data.featured_image + ")";
                $(".zoomWindowContainer>div ,.zoomWindow").css("background-image", image_loc);
                // console.log(image_loc);
                // $.each(data.another_images,function(key,value){
                //     console.log("key"+key+"| Value:"+value.file_location+value.file_fullname);
                // });
                var i = 0;
                $('.gallery-zoom-set').each(function () {
                    console.log(base_url + data.another_images[i].file_location + data.another_images[i].file_fullname);
                    $(this).attr({
                        "data-image": base_url + data.another_images[i].file_location + data.another_images[i].file_fullname,
                        "data-zoom-image": base_url + data.another_images[i].file_location + data.another_images[i].file_fullname
                    });
                    $(this).find("img").attr("src", base_url + data.another_images[i].file_location + data.another_images[i].file_fullname);
                    i = i + 1;
                })
            }
        });
    });
    //Custom transaction in radio button on merchandise
    $("input[name=choice]").click(function () {
        var selectorId = $(this).val();
        // console.log('input[name='+selectorId+']');
        var option_val = $('select[name=' + selectorId + ']').val().split("|");
        var prod_id = option_val[0];
        var prod_price = option_val[1];
        $('.opt-select').prop("disabled", true);
        $('select[name=' + selectorId + ']').prop("disabled", false);
        $(".selected-buy").attr('data-id', prod_id);
        $(".selected-price").html("IDR " + idrFormatNumber(prod_price));
        $.ajax({
            type: 'POST',
            data: {id: prod_id},
            url: base_url + 'landing/generateZoomedImage',
            success: function (response) {
                var data = JSON.parse(response);
                $("#zoom3").attr({
                    "src": base_url + "asset/upload/images/" + data.featured_image,
                    "data-zoom-image": base_url + "asset/upload/images/" + data.featured_image
                });
                var image_loc = "url(" + base_url + "asset/upload/images/" + data.featured_image + ")";
                $(".zoomWindowContainer>div ,.zoomWindow").css("background-image", image_loc);
                // console.log(image_loc);
                // $.each(data.another_images,function(key,value){
                //     console.log("key"+key+"| Value:"+value.file_location+value.file_fullname);
                // });
                var i = 0;
                $('.gallery-zoom-set').each(function () {
                    // console.log(base_url+data.another_images[i].file_location+data.another_images[i].file_fullname);
                    $(this).attr({
                        "data-image": base_url + data.another_images[i].file_location + data.another_images[i].file_fullname,
                        "data-zoom-image": base_url + data.another_images[i].file_location + data.another_images[i].file_fullname
                    });
                    $(this).find("img").attr("src", base_url + data.another_images[i].file_location + data.another_images[i].file_fullname);
                    i = i + 1;
                })
            }
        });
    });
    $(".opt-select").change(function () {
        var option_val = $(this).val().split("|");
        var prod_id = option_val[0];
        var prod_price = option_val[1];
        $(".sel-buy").attr('data-id', prod_id);
        $(".selected-price").html("IDR " + idrFormatNumber(prod_price));
    });
    // PopUp Detail
    $('.buy').click(function (e) {
        e.preventDefault();
         $('#confirm_reveal').html('Loading...').foundation('open');
        qty = $('input[name=qty]').val();
        product_id=$(this).attr('data-id');
        parent_id=$(this).attr('data-parent');
        var dataString = {
            product_id : product_id,
            parent_id : parent_id,
            qty : qty,
        }
        $.ajax({
            type: "POST",
            url: base_url+'AjaxController/CheckProductSubscribe/'+parent_id,
            data: dataString,
            dataType: "json",
            cache : false,
            success: function(data){
               var emagzs=[];
               if($('#emagzs').val())
                    emagzs=$('#emagzs').val();
                var cmagz=parseInt(emagzs.length)+parseInt(data.month);
               if(cmagz!=0){
                $('#confirm_reveal').html('<div class="grid-x padding-top-1"><div class="medium-12 cell"><center><h5 class="padding-top-1">Selected edition should be '+Math.abs(data.month)+'</h5></center></div></div><div class="padding-top-1"></div><button class="close-button" data-close="" aria-label="Close modal" type="button"><span aria-hidden="true">×</span></button>').foundation('open');
               }else{  

                    $.ajax({
                        type: "POST",
                        url: base_url+'cart/check_active_benefit',
                        data: dataString,
                        dataType: "json",
                        cache : false,
                        success: function(data){
                            if(data.status=="tanpa_benefit"){
                                modal_content('confirm_modal/confirm_cart');
                                buybutton(dataString.product_id,dataString.parent_id,qty);
                            }
                            if(data.status=="success"){
                            modal_content('confirm_modal/confirm_cart');
                            buybutton(dataString.product_id,dataString.parent_id,qty);
                            }
                            if(data.status=="error_beda_category"){
                                var holla=confirm(data.message);
                                if (holla==true) {
                                    $.ajax({
                                        type: 'POST',
                                        url: base_url+'Member/destroy_benefit',
                                        success: function (data) {
                                            modal_content('confirm_modal/confirm_cart');
                                            buybutton(dataString.product_id,dataString.parent_id,qty);
                                        }
                                    });
                                }
                            }
                            if(data.status=="change_item"){
                                var holla=confirm(data.message);
                                if (holla==true) {
                                    $.ajax({
                                        type: 'POST',
                                        url: base_url+'cart/destroycart',
                                        success: function (data) {
                                        buybutton(dataString.product_id,dataString.parent_id,qty);
                                        $(location).attr('href',base_url+'cart/' );
                                        }
                                    });
                                }
                            }
                            if(data.status=="error_price_over"){
                                var holla=confirm(data.message);
                                if (holla==true) {
                                    $.ajax({
                                        type: 'POST',
                                        url: base_url+'Member/destroy_benefit',
                                        success: function (data) {
                                           buybutton(dataString.product_id,dataString.parent_id,qty);
                                        }
                                    });
                                }
                            }

                         } ,error: function(xhr, status, error) {
                          alert(error);
                        },
                    });
               }
            }
        }); 
    });
    function buybutton(productid,parentid,quantity){
        qty =quantity;
        product_id=productid;
        parent_id=parentid;
        emagzs=$('#emagzs').val();
        if(!$('#emagzs').val()){
            emagzs=0;
        }
        if (!qty) {
            qty = 1;
        }
        $.ajax({
            type: 'POST',
            data: {id: product_id, parentID:parent_id, qty: qty, emagzs:emagzs},
            url: base_url + 'cart/buy',
            success: function (response) {
                refreshCart();
            }
        });
    }
    function modal_content(url){
        var $modal = $('#confirm_reveal');
        $.ajax(base_url+url)
        .done(function(resp){
        $modal.html(resp).foundation('open');
        });
    }
    // Popup Certificate Detail
    $('.certDetailBtn').click(function (e) {
        e.preventDefault();
        var certIdData = $(this).attr('data-certId');
        var certName = $(this).attr('data-certName');
        $('#modalHeader').html(certName);

        $.ajax({
            type: 'POST',
            data: {certId: certIdData},
            url: base_url + 'certificate/getCertificateDetailbyUserLoggedIn',
            success: function (r) {
                $('.mshop-modal').modal();
                $('#modalBody').html(r);
            }
        });
    });

    // refresh cart in header
    function refreshCart() {
        $.ajax({
            type: 'POST',
            data: {id: $(this).attr('data-id'), parentID: $(this).attr('data-parent'), qty: $('input[name=qty]').val()},
            url: base_url + 'cart/refreshCart',
            success: function (response) {
                if (parseInt(response) > 0) {
                    $('.cart-button').addClass('bgMagenta');
                } else {
                    $('.cart-button').removeClass('bgMagenta');
                }
                $('.badge-cart').html(response);
            }
        });
    };
    //Iqbal check benefit category
    $(document).on("click", ".check_benefit_redeem", function() {
        var product_id=$(this).attr('product-id');
        var dataString = {
            product_id : product_id,
        };
        var location_page=$(this).attr('location');
        $.ajax({
            type: 'POST',
            data: dataString,
            dataType: "json",
            cache : false,
            url: base_url+'Member/check_active_benefit',
            success: function (data) {
                if(data.status=="error_beda_category"){
                    var holla=confirm(data.message);
                    if (holla==true) {
                        $.ajax({
                            type: 'POST',
                            url: base_url+'Member/destroy_benefit',
                            success: function (data) {
//                                window.location=location_page;
                                    showModalTicket(location_page);
                            }
                        });
                    }else{
                        return false;
                    }
                }
                if(data.status=="success"){
//                    window.location=location_page;
                    showModalTicket(location_page);
                }
//                window.location=location_page;
                showModalTicket(location_page);
            }
        });
    });
    function check_benefit_active(productid,parentid,handledata){
        var dataString = {
            product_id : product_id,
            parent_id  : parentid,
        };
        $.ajax({
            type: 'POST',
            data: dataString,
            dataType: "json",
            cache : false,
            url: base_url+'Member/check_active_benefit',
            success: function (data) {
                handleData(data);
            }
        });

    }
    // IQBAL chect jenis benefit program apakah free atau buy one get one;
    $(document).on("click", ".order_button", function() {
        var hollaa=$('#form_ticket').serialize();
        $.ajax({
            type: 'POST',
            data: hollaa,
            dataType: "json",
            cache : false,
            url: base_url+'Member/check_benefit_type',
            success: function (data) {
               if(data.status=='error'){
                    alert(data.message);
               }else{
                $( "#form_ticket" ).submit();
               }
            }
        });
    });
    //initiate the plugin and pass the id of the div containing gallery images


    // Date Picker
    $('.datepickerAdvSearch').datepicker({
        autoclose: true,
        format: 'MM yyyy',
        showButtonPanel: true,
        viewMode: "months",
        minViewMode: "months",
    });
    // just add class datepicker general -Added by Adrian Feb, 21 2017
    $('.datepickerGeneral').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        showButtonPanel: true,
    });
    // ***********

    $("a.dropdown-toggle, button.dropdown-toggle").click(function () {
        $(this).parent().find(".dropdown-menu").slideToggle("800");
    });

    /*
     * Tax Invoice Request Form
     * Ajax handler to choose setting
     *
     * @author  Sutisna
     * @date        6 January 2017
     */
    $('.taxRequestChooseSetting').change(function (e) {
        selectedVal = $(this).val();
        $.ajax({
            url: base_url + 'tax/getDetailSetting/' + selectedVal,
            success: function (e) {
                if (e.code == "200") {
                    $('#TaxInvoiceReqForm input[name="npwp"]').val(e.data.npwp);
                    if (e.data.npwp_file) {
                        npwpImageFile = base_url + e.data.npwp_file;

                        //Generate image to show
                        npwpImage = "<a href='" + npwpImageFile + "' target='_blank'>";
                        npwpImage = npwpImage + "<img src='" + npwpImageFile + "' width='200'/>";
                        npwpImage = npwpImage + "</a>";
                        $('#TaxInvoiceReqForm .containerImageNpwp').html(npwpImage);

                        $('#TaxInvoiceReqForm input[name="npwp_file_hidden"]').val(e.data.npwp_file);
                    }
                    $('#TaxInvoiceReqForm input[name="requester_name"]').val(e.data.requester_name_default);
                    $('#TaxInvoiceReqForm input[name="requester_nemail"]').val(e.data.requester_email_default);
                } else {
                    alert(e.message);
                }
            },
            dataType: 'json'
        });
    });
    // Pagination per page selection handler
    // @author Adrian
    // @date 28 February 2017
    $('.sel_pagination').change(function () {
        var perPage = $(this).val();
        var usage = $(this).data("usage");
        console.log(usage);
        $.ajax({
            type: 'POST',
            data: {select_perPage: perPage, usage: usage},
            url: base_url + "Landing/setPagingCard",
            beforeSend: function (e) {
                $(".paging-loading").css("display", "inline-block");
                $('.sel_pagination').prop("disabled", "disabled");
            },
            success: function (e) {
                location.reload();
            }
        });
    });
    // Deprecated Adrian
    // $('.sel_paginationLarge').change(function(){
    //   var perPage=$(this).val();
    //   $.ajax({
    //     type:'POST',
    //     data:{select_perPage:perPage},
    //     url: base_url+"Landing/setPagingCardLarge",
    //       beforeSend:function(e){
    //           $(".paging-loading").css("display","inline-block");
    //           $('.sel_paginationLarge').prop("disabled","disabled");
    //       },
    //     success:function(e){
    //       location.reload();
    //     }
    //   });
    // });
    // **********

    // Shipping and billing handler
    // @author Adrian
    // @date 1 March 2017
    $('.use_address').click(function () {
        var addr_id = $(this).attr('data-id');
        var usage = $(this).attr('data-usage');
        $.ajax({
            type: 'POST',
            data: {id: addr_id, usage: usage},
            url: base_url + 'Member/setAddress',
            beforeSend: function (response) {
                $('.use_address[data-id=' + addr_id + ']').button('loading');
            },
            success: function (response) {
                if (response == 'success') {
                    if (usage == 'shipping') {
                        $.notify({
                            // options
                            icon: 'glyphicon glyphicon-info-sign',
                            message: 'Address has been set as main shipping address, redirect in a moment'
                        }, {
                            // settings
                            notif_option
                        });
                    } else {
                        $.notify({
                            // options
                            icon: 'glyphicon glyphicon-info-sign',
                            message: 'Address has been set as main billing address, redirect in a moment'
                        }, {
                            // settings
                            notif_option
                        });
                    }

                    location.reload();
                } else {
                    $('.use_address[data-id=' + addr_id + ']').button('reset');
                }

            }
        });
    });
    $('.delete_address').click(function () {
        var addr_id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            data: {id: addr_id},
            url: base_url + 'Member/delAddress',
            beforeSend: function (response) {
                $('.delete_address[data-id=' + addr_id + ']').button('loading');
            },
            success: function (response) {
                if (response == 'success') {
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-info-sign',
                        message: 'Address deleted successfully, redirect in a moment'
                    }, {
                        // settings
                        notif_option
                    });
                    location.reload();
                } else {
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-info-sign',
                        message: 'Address has been used as your main shipping or billing address, Please choose another address item'
                    }, {
                        // settings
                        type: 'warning',
                        timer: 5000,
                        delay: 1000,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    $('.delete_address[data-id=' + addr_id + ']').button('reset');
                }

            }
        });
    });
    // Added to use arrow rotated effect- Adrian Mar 6 2017
    $("#advanceSearch-mobile").click(function (e) {
        $(".adv-arrow-right").toggleClass('rotated-right');
    });

    // Added to disable category Training on Certification Category in Advance Search
    $(".trainingCategory").change(function () {
        var thisCat = $(this).val();
        var certIdArray = ["170", "171", "172"]; //certification id value
        if (certIdArray.includes(thisCat)) {
            $(this).parent().find("select#trainingProgram").val("");
            $(this).parent().find("select#trainingProgram").attr("disabled", true);
        } else {
            $(this).parent().find("select#trainingProgram").attr("disabled", false);
        }
    });
    // added to show certifiacte modal on credit information clicked
    $(".credit_information").click(function () {
        $.ajax({
            type: 'POST',
            url: base_url + 'Landing/display_certificateInformation',
            success: function (response) {
                $(".mshop-modal").modal('show');
                $(".mshop-modal").addClass('modalLarge');
                $(".mshop-modal #modalBody").html(response);
            }
        });
    });
    $(".mshop-modal").on("hidden.bs.modal", function () {
        $(".mshop-modal").removeClass('modalLarge');
    });
    $("#training_list th span").click(function () {
        var orderBy = $(this).parent('th').attr('data-order');
        var orderValue = 'asc';
        var oldUrl = window.location.href;
        if (oldUrl == window.location.href) {
            oldUrl += "?"
        }
        var oldUrlSplited = oldUrl.split('?');
        oldUrlSplited = oldUrlSplited[1].split('&');
        console.log(oldUrlSplited.length);
        var newUrl;
        if (oldUrlSplited.length > 1) {
            for (var i = 0; i < oldUrlSplited.length; i++) {
                newSplit = oldUrlSplited[i].split('=');
                if (newSplit[0] == 'order_by' || newSplit[0] == 'order_value') {
                    if (newSplit[0] == 'order_value' && newSplit[1] == 'asc') {
                        orderValue = 'desc'
                    }
                } else {
                    if (i == 0) {
                        newUrl = oldUrlSplited[i];
                    } else {
                        newUrl += '&' + oldUrlSplited[i];
                    }
                }
            }
            newUrl += '&order_by=' + orderBy + '&order_value=' + orderValue
        } else {
            newUrl = 'order_by=' + orderBy + '&order_value=' + orderValue
        }
        // console.log(newUrl)
        if(orderBy){
            window.location.search = newUrl
        }
    });
     $("#summaryBtnSubmit").click(function (e) {
        var dataString = {
            source_from : $('.source_from').val(),
            source_value_sales : $('.source_value_sales').val(),
            source_value_other : $('.source_value_other').val(),
        };
         e.preventDefault();
         $("#summaryBtnSubmit").button("loading");
            setTimeout(function () {
            $.ajax({
                type: "POST",
                url: base_url+'summary/submit_form_source',
                data: dataString,
                dataType: "json",
                cache : false,
                success: function(data){
                    if(data.status =='success'){
                       $("#formToPay").submit();
                    }
                     if(data.status =='error'){
                        $("#summaryBtnSubmit").button("reset");
                       alert('please fill form');
                       return false;
                    }
                } ,error: function(xhr, status, error) {
                        $( "#load" ).hide();
                    alert(error);
                },
            });
        }, 3000)

    });
    $("#zoom3").elevateZoom({
        constrainType: "height",
        constrainSize: 500,
        gallery: 'gallery',
        imageCrossfade: true,
        cursor: 'pointer',
        easing: true,
        zoomWindowWidth: 300,
        zoomWindowHeight: 300,
        galleryActiveClass: "active"
    });
    $("#zoom_ticket").elevateZoom({
        constrainType: "height",
        constrainSize: 700,
        scrollZoom: true,
        easing: true,
        imageCrossfade: true,
        cursor: 'pointer',
        zoomWindowWidth: 300,
        zoomWindowHeight: 300,
    });
    $(".zoomed").elevateZoom({
        scrollZoom: true,
        easing: true,
        imageCrossfade: true,
        cursor: 'pointer',
        zoomWindowWidth: 300,
        zoomWindowHeight: 300,
    });

    $('.child-prod-slick').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                }
            },
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
        prevArrow: '<button type="button" data-role="none" class="slick-prev icon-angle-left slick-arrow" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next icon-angle-right slick-arrow" aria-label="Next" tabindex="0" role="button">Next</button>',
    })

    $('.course-location').slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                }
            },
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
        prevArrow: '<button type="button" data-role="none" class="slick-prev icon-angle-left slick-arrow" aria-label="Previous" tabindex="0" role="button">Previous</button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next icon-angle-right slick-arrow" aria-label="Next" tabindex="0" role="button">Next</button>',

    })
      $('.product-slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        nextArrow: '<button type="button" class="slick-next icon-angle-right" style="color: black;">Next</button>',
        prevArrow: '<button type="button" class="slick-prev icon-angle-left" style="color: black;">Prev</button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: "unslick"
            }
        ]
    });
    $('.featured-slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        nextArrow: '<button type="button" class="slick-next icon-angle-right" style="color: black;">Next</button>',
        prevArrow: '<button type="button" class="slick-prev icon-angle-left" style="color: black;">Prev</button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: "unslick"
            }
        ]
    });

});

function setTicket(id,text) {
    // console.log(id);
    var res = id.split("|");
    $("#td"+res[1]).text(text);
    var input, filter, table, tr, td, i;
    // input = document.getElementById('');
    var sum = 0;
    var tid = 0;
    $('.setTicket').each(function(){
        tid=$(this).val().split("|");
        console.log(' csa'+tid[0]);
        // if(tid[0]){
        sum = sum+parseFloat(tid[0]);  // Or this.innerHTML, this.innerText
            
        // }
    });
    filter = sum;//res[0];//input.value.toUpperCase();
    console.log(filter);
    table = document.getElementById("ticketsBundles");
    tr = table.getElementsByTagName("tr");
    // $("#bundleIds").text(filter);
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
    }
    // console.log("#td"+res[1]);
}

//Start of Zendesk Chat Script
// window.$zopim||(function(d,s){var z=$zopim=function(c){
// z._.push(c)},$=z.s=
// d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
// _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
// $.src='https://v2.zopim.com/?4tDDqVLLxN5j916m1RlPKuPJzpiG3fyP';z.t=+new Date;$.
// type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
