$(document).foundation();


// Category card-reveal
// Open Card Reveal Hover
// $('.open-button').hover(function(){
//   $(this).siblings('.card-reveal').addClass('open');
//   }
// );
// // Close Card Reveal Hover
// $('.close-button').click(function(){
//   $(this).parent().parent('.card-reveal').removeClass('open');
// });

// $(".open-button")
// .mouseenter(function(){

//   $(this).siblings('.card-reveal').show();

// })
// .mouseleave(function(){
//     $(this).siblings('.card-reveal').hide();
// });

// $(".open-button").mouseover(function () {
//   $(this).siblings('.card-reveal').addClass('open');
// });
// $(".card-reveal").mouseover(function () {
//   $(this).removeClass('open');
// });


$(".open-button img, .open-button").mouseenter(function() {
  $(this).parent().children('.card-reveal').show("slide", { direction: "up" }, 400);
});

$(".card-reveal").mouseleave(function() {
  $(this).parent().children('.card-reveal').hide("slide", { direction: "up" }, 400);
});


// Fixing Menu hover issue

 $("#tabs-public-training,#sub-menu-training").mouseover(function () {
   $('#sub-menu-training').removeClass('hide-sub-menu');
 }).mouseleave(function() {
     $('#sub-menu-training').addClass('hide-sub-menu');
 });




// Filter Section
//More (Expand) or Less (Collapse)
$('.categories-menu.menu.nested').each(function(){
  var filterAmount = $(this).find('li').length;
  if( filterAmount > 5){
    $('li', this).eq(4).nextAll().hide().addClass('toggleable');
    $(this).append('<li class="more">More</li>');
  }
});

$('.categories-menu.menu.nested').on('click','.more', function(){
  if( $(this).hasClass('less') ){
    $(this).text('More').removeClass('less');
  }else{
    $(this).text('Less').addClass('less');
  }
  $(this).siblings('li.toggleable').slideToggle();
});



// Featured Training Section
$(document).ready(function () {
  $('#hover-feautred-training-a').hover(function () {
      $('#hover-feautred-training').addClass('featured-training-a',550);
  }, function () {
      $('#hover-feautred-training').removeClass('featured-training-a', 550);
  });

  $('#hover-feautred-training-b').hover(function () {
    $('#hover-feautred-training').addClass('featured-training-b', 550);
  }, function () {
      $('#hover-feautred-training').removeClass('featured-training-b', 550);
  });

  $('#hover-feautred-training-c').hover(function () {
    $('#hover-feautred-training').addClass('featured-training-c',550);
  }, function () {
      $('#hover-feautred-training').removeClass('featured-training-c', 550);
  });

});


// Scroll hiding Topbar (Menu)
var prev = 0;
var $window = $(window);
var nav = $('.scrollhide-nav');

$window.on('scroll', function(){
  var scrollTop = $window.scrollTop();
  nav.toggleClass('hidden', scrollTop > 140);
  prev = scrollTop;
});

// Hide sub menu when mouseout
// $('.sub-menu').on('mouseout',function(){
//   $(this).removeClass('show-sub-menu');
// });

// Wizard breadcrumb
$('.breadcrumb-counter-nav-item').click(function () {
  $('.breadcrumb-counter-nav-item').removeClass('current');
  $(this).addClass('current');
});

// Select2 JS
$( document ).ready(function() {
  $('.select2').select2({
    theme: "foundation"
  });
});


// Date Picker Month
$(function(){
  $('#dpMonthsFrom').fdatepicker({    leftArrow:'<<',
    rightArrow:'>>',});
  $('#dpMonthsUntil').fdatepicker({   leftArrow:'<<',
    rightArrow:'>>',});
});

// Dependent Date Picker
$( document ).ready(function() {
  var nowTemp = new Date();
  var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
  var checkin = $('.dpd1').fdatepicker({
        leftArrow:'<<',
    rightArrow:'>>',
    onRender: function (date) {
      return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function (ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
      var newDate = new Date(ev.date)
      newDate.setDate(newDate.getDate() + 1);
      checkout.update(newDate);
    }
    checkin.hide();
    $('.dpd2')[0].focus();
  }).data('datepicker');
  var checkout = $('.dpd2').fdatepicker({
        leftArrow:'<<',
    rightArrow:'>>',
    onRender: function (date) {
      return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
  }).on('changeDate', function (ev) {
    checkout.hide();
  }).data('datepicker');
});


$(document).ready(function() {

  $("#submitPromocodeId").on('click', function() {
confirm("<h1>Use Promo code \""+$('input[name=promocode]').val()+"\" ?</h1>","<p>\"Promo code can not be combined with another promotion\"</p>", function() {
        var code = $('input[name=promocode]').val();
        $.ajax({
            type: 'POST',
            data: {code: code},
            url: base_url + 'admin/promocode/checkPromocodeAjax',
            success: function (response) {
                var result = JSON.parse(response);
                if (result.status) {
                  confirm("This promocode have a minimum purchase, are you sure will use this ?","", function() {
                        $("#submitPromocode").button("loading");
                        $('#promocode').submit();
                  });
                } else {
                  confirm("Are you sure you want to use this Promocode ?","", function() {
                        $("#submitPromocode").button("loading");
                        $('#promocode').submit();
                });
                }
            }
        });
    });
  });

  $(".btn_proceed_agree").on('click', function() {
    if (!$('.agreeCB').is(":checked"))
    {
      ownAlert("<h1>Warning</h1>","<p>\"Please tick the terms & conditions box to proceed\"</p>", function() {
              
        
      });
    }
  });

  $("#requestPayment").on('click', function() {
      $.ajax({
          type: 'POST',
          data: {email: $('input[name=EMAIL]').val()},
          url: base_url + 'summary/check_verify',
          success: function (response) {
            if(response==='1'){
               $('#formToPay').submit();
            }else{
              ownAlert("<h1>This is Important!</h1>","<p>\"You can continue shopping by verify your email. Please check your inbox.\"</p>", function() {});
            }
          }
      });
   });

  function confirm(title, message, callback) {
    var modal = '<div class="reveal small" id="confirmation" data-close-on-click="false" data-reveal data-animation-in="slide-in-up">'+
            '<h2>'+title+'</h2>'+
            '<p class="lead">'+message+'</p>'+
            '<div class="grid-x flex-container align-right" id="confirmationBody">'+
                    '<button class="button small cancel-button hvr-sweep-to-right margin-right-1" data-close>No</button>'+
                    '<button class="button small marketeers hvr-sweep-to-right yes">Yes</button>'+
                    '</div>'+
                    '<button class="close-button" data-close aria-label="Close modal" type="button">'+
                    '<span aria-hidden="true">&times;</span>'+
                    '</button>'+
              '</div>';
    $('body').append(modal);
    var confirmation = new Foundation.Reveal($('#confirmation'));
    confirmation.open();
    $('#confirmationBody').children('.yes').on('click', function() {
      confirmation.close();
      $('#confirmation').remove();
        callback.call();
      });
    $(document).on('closed.zf.reveal', '#confirmation', function() {
      $('#confirmation').remove();
    });

  }

  function ownAlert(title, message, callback) {
    var modal = '<div class="reveal small" id="confirmation" data-close-on-click="false" data-reveal data-animation-in="slide-in-up">'+
    '<h2>'+title+'</h2>'+
    '<p class="lead">'+message+'</p>'+
//    '<div class="grid-x flex-container align-right" id="confirmationBody">'+
//    '<button class="button small cancel-button hvr-sweep-to-right margin-right-1" data-close>OK</button>'+
//    '</div>'+
    '<button class="close-button" data-close aria-label="Close modal" type="button">'+
    '<span aria-hidden="true">&times;</span>'+
    '</button>'+
    '</div>';
    $('body').append(modal);
    var confirmation = new Foundation.Reveal($('#confirmation'));
    confirmation.open();
    $('#confirmationBody').children('.yes').on('click', function() {
      confirmation.close();
      $('#confirmation').remove();
      callback.call();
    });
    $(document).on('closed.zf.reveal', '#confirmation', function() {
      $('#confirmation').remove();
    });

  }

    $('.update-cart-v2').click(function () {
        var qty_sent = $(this).attr('data-qty');
        var row_data = $(this).attr('data-row');
        var promocode_allProd = $(".promocode_allProd").attr('value');
        if ($(this).attr('data-qty') == 999) {
            if ($("input." + row_data).val() != $("input.qty_temp" + row_data).val()) {
                qty_sent = $("input." + row_data).val() - $("input.qty_temp" + row_data).val();
            }
            else {
                qty_sent = 0;
            }
            $.ajax({
                type: 'POST',
                data: {id: $(this).attr('data-id'), qty: qty_sent, rowid: row_data},
                url: base_url + 'cart/updateCart',
                success: function (response) {
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-info-sign',
                        url: base_url + 'cart',
                        target: '_blank',
                        message: 'Please wait, your cart is being updated'
                    }, {
                        // settings
                        notif_option
                    });
                    // $('.response').html('Cart has been updated');
                    // $('.response').slideDown('fast').delay('3000').slideUp('fast');
                    location.reload();
                }
            });
        }
        else if ($(this).attr('data-qty') == -404) {
            Lobibox.confirm({
                title: "Remove Product",
                msg: "Are you sure you want to remove selected item from cart?",
                modal: true,
                callback: function ($this, type, ev) {
                    if (type == 'yes') {
                        $.ajax({
                            type: 'POST',
                            data: {id: $(this).attr('data-id'), qty: qty_sent, rowid: row_data},
                            url: base_url + 'cart/updateCart',
                            success: function (response) {
                                // $.notify({
                                // // options
                                //    icon: 'glyphicon glyphicon-info-sign',
                                //    message: 'Cart has been updated'
                                // },{
                                // // settings
                                //    notif_option
                                // });
                                $('.response').html('Cart has been updated');
                                $('.response').slideDown('fast').delay('3000').slideUp('fast');
                            }
                        });
                        // alert(link);
                        $.ajax({
                            type: 'POST',
                            data: {promocode_all: promocode_allProd},
                            url: base_url + 'admin/promocode/updatePromocode_allProd',
                            success: function (response) {
                                location.reload();
                            }
                        });
                    }
                    else {
                        return false;
                    }
                }
            })
        }
    });


    $('.regSubmitBtnV2').click(function (e) {
        e.preventDefault();
        var regEmail = $('.regEmail').val();
        $.ajax({
            type: 'POST',
            data: {ForgetPasswordEmail: regEmail},
            url: base_url + 'member/is_emailExistedAjax',
            beforeSend: function () {
                $('.regSubmitBtn').button('loading');
            },
            success: function (response) {
                var result = JSON.parse(response);
                if (result.status == "EmailFound") {
                  ownAlert("Email already registered","", function() {});
                } else {
//                  ownAlert("Register process...","", function() {});

                    $("#registerFormCheck").submit();
                }
            }
        });
    });

// // Loader
// var delayVar;

// function myFunction() {
//   delayVar = setTimeout(showPage, 2000);
// }

// function showPage() {


// }
document.onreadystatechange = function () {
  var state = document.readyState
  if (state == 'interactive') {
//       document.getElementById('contents').style.visibility="hidden";
  } else if (state == 'complete') {
      setTimeout(function(){
      // document.getElementById("loader").style.display = "none";
//      document.getElementById("imgLoader").style.display = "none";
//      document.getElementById("mainBody").style.display = "block";
        // document.getElementById("overlay").style.display = "none";
      },200);
  }
}

});

//Merchandise Cover
$('.sim-thumb').on('click', function() {
$('#main-product-image').attr('src', $(this).data('image'));
})


//Elevate Zoom
$("#zoom_01").elevateZoom();


function showModalTicket(url){
    $('#ticket_url').val(url);
    $('#btnMdlTicket').trigger('click');
}
$('#reveal1').on('open.zf.reveal',function(){
    $.ajax($('#ticket_url').val())
    .done(function(resp){
      console.log('done');
        $('#reveal2').html(resp);
        $('#reveal2').trigger('resizeme.zf.reveal');
    });
});
$('#reveal1').on('close.zf.reveal',function(){

        $('#reveal2').html('<h3>Loading...</h3>');
        $('#reveal2').trigger('resizeme.zf.reveal');

});
$('.close-reveal-modal').click(function(){
    // $('#modal2').foundation('close'); didn't work for some reason.
    // 'open' closes all other modals, so it accomplishes what I need.
//   $('#reveal1').foundation('reveal', 'close');
//    window.location.reload();
$('.reveal-overlay').trigger('click');
//  $('.reveal-overlay').remove();
//  $('.reveal').remove();
});