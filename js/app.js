$(document).foundation();

$(".open-button img, .open-button").mouseenter(function() {
  $(this)
    .parent()
    .children(".card-reveal")
    .show("slide", { direction: "up" }, 400);
});

$(".card-reveal").mouseleave(function() {
  $(this)
    .parent()
    .children(".card-reveal")
    .hide("slide", { direction: "up" }, 400);
});

// Fixing Menu hover issue

$("#tabs-public-training,#sub-menu-training")
  .mouseover(function() {
    $("#sub-menu-training").removeClass("hide-sub-menu");
  })
  .mouseleave(function() {
    $("#sub-menu-training").addClass("hide-sub-menu");
  });

// Filter Section
//More (Expand) or Less (Collapse)
$(".categories-menu.menu.nested").each(function() {
  var filterAmount = $(this).find("li").length;
  if (filterAmount > 5) {
    $("li", this)
      .eq(4)
      .nextAll()
      .hide()
      .addClass("toggleable");
    $(this).append('<li class="more">More</li>');
  }
});

$(".categories-menu.menu.nested").on("click", ".more", function() {
  if ($(this).hasClass("less")) {
    $(this)
      .text("More")
      .removeClass("less");
  } else {
    $(this)
      .text("Less")
      .addClass("less");
  }
  $(this)
    .siblings("li.toggleable")
    .slideToggle();
});

// Featured Training Section
$(document).ready(function() {
  $("#hover-feautred-training-a").hover(
    function() {
      $("#hover-feautred-training").addClass("featured-training-a", 550);
    },
    function() {
      $("#hover-feautred-training").removeClass("featured-training-a", 550);
    }
  );

  $("#hover-feautred-training-b").hover(
    function() {
      $("#hover-feautred-training").addClass("featured-training-b", 550);
    },
    function() {
      $("#hover-feautred-training").removeClass("featured-training-b", 550);
    }
  );

  $("#hover-feautred-training-c").hover(
    function() {
      $("#hover-feautred-training").addClass("featured-training-c", 550);
    },
    function() {
      $("#hover-feautred-training").removeClass("featured-training-c", 550);
    }
  );
});

// Scroll hiding Topbar (Menu)
var prev = 0;
var $window = $(window);
var nav = $(".scrollhide-nav");

$window.on("scroll", function() {
  var scrollTop = $window.scrollTop();
  nav.toggleClass("hidden", scrollTop > prev);
  prev = scrollTop;
});

// Wizard breadcrumb
$(".breadcrumb-counter-nav-item").click(function() {
  $(".breadcrumb-counter-nav-item").removeClass("current");
  $(this).addClass("current");
});

// Select2 JS
$(document).ready(function() {
  $(".select2").select2({
    theme: "foundation"
  });
});

// Date Picker Month
$(function() {
  $("#dpMonthsFrom").fdatepicker();
  $("#dpMonthsUntil").fdatepicker();
});

// Dependent Date Picker
$(document).ready(function() {
  var nowTemp = new Date();
  var now = new Date(
    nowTemp.getFullYear(),
    nowTemp.getMonth(),
    nowTemp.getDate(),
    0,
    0,
    0,
    0
  );
  var checkin = $("#dpd1")
    .fdatepicker({
      onRender: function(date) {
        return date.valueOf() < now.valueOf() ? "disabled" : "";
      }
    })
    .on("changeDate", function(ev) {
      if (ev.date.valueOf() > checkout.date.valueOf()) {
        var newDate = new Date(ev.date);
        newDate.setDate(newDate.getDate() + 1);
        checkout.update(newDate);
      }
      checkin.hide();
      $("#dpd2")[0].focus();
    })
    .data("datepicker");
  var checkout = $("#dpd2")
    .fdatepicker({
      onRender: function(date) {
        return date.valueOf() <= checkin.date.valueOf() ? "disabled" : "";
      }
    })
    .on("changeDate", function(ev) {
      checkout.hide();
    })
    .data("datepicker");
});

// Merchandise Cover
$(".sim-thumb").on("click", function() {
  $("#main-product-image").attr("src", $(this).data("image"));
});

// Elevate Zoom
$("#zoom_01").elevateZoom();

// Loader
var delayVar;

function myFunction() {
  delayVar = setTimeout(showPage, 2000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("imgLoader").style.display = "none";
  document.getElementById("mainBody").style.display = "block";
}

// Sticky Footer
var MutationObserver = (function() {
  var prefixes = ["WebKit", "Moz", "O", "Ms", ""];
  for (var i = 0; i < prefixes.length; i++) {
    if (prefixes[i] + "MutationObserver" in window) {
      return window[prefixes[i] + "MutationObserver"];
    }
  }
  return false;
})();

window.onload = function() {
  stickyFooter();

  if (MutationObserver) {
    observer.observe(target, config);
  } else {
    //old IE
    setInterval(stickyFooter, 500);
  }
};

//check for changes to the DOM
var target = document.body;
var observer;
var config = {
  attributes: true,
  childList: true,
  characterData: true,
  subtree: true
};

if (MutationObserver) {
  // create an observer instance
  observer = new MutationObserver(mutationObjectCallback);
}

function mutationObjectCallback(mutationRecordsList) {
  stickyFooter();
}

//check for resize event
window.onresize = function() {
  stickyFooter();
};

//lets get the marginTop for the <footer>
function getCSS(element, property) {
  var elem = document.getElementsByTagName(element)[0];
  var css = null;

  if (elem.currentStyle) {
    css = elem.currentStyle[property];
  } else if (window.getComputedStyle) {
    css = document.defaultView
      .getComputedStyle(elem, null)
      .getPropertyValue(property);
  }

  return css;
}

function stickyFooter() {
  if (MutationObserver) {
    observer.disconnect();
  }
  document.body.setAttribute("style", "height:auto");

  //only get the last footer
  var footer = document.getElementsByTagName("footer")[
    document.getElementsByTagName("footer").length - 1
  ];

  if (footer.getAttribute("style") !== null) {
    footer.removeAttribute("style");
  }

  if (window.innerHeight != document.body.offsetHeight) {
    var offset = window.innerHeight - document.body.offsetHeight;
    var current = getCSS("footer", "margin-top");

    if (isNaN(parseInt(current)) === true) {
      footer.setAttribute("style", "margin-top:0px;");
      current = 0;
    } else {
      current = parseInt(current);
    }

    if (current + offset > parseInt(getCSS("footer", "margin-top"))) {
      footer.setAttribute("style", "margin-top:" + (current + offset) + "px;");
    }
  }

  document.body.setAttribute("style", "height:100%");

  //reconnect
  if (MutationObserver) {
    observer.observe(target, config);
  }
}

// Popup WhatsApp
$(".popout .btn").click(function() {
  $(this).toggleClass("active");
  $(this)
    .closest(".popout")
    .find(".panel")
    .toggleClass("active");
});
$(document).click(function() {
  $(".popout .panel").removeClass("active");
  $(".popout .btn").removeClass("active");
});
$(".popout .panel").click(function(event) {
  event.stopPropagation();
});
$(".popout .btn").click(function(event) {
  event.stopPropagation();
});
